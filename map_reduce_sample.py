import concurrent.futures
import json
import math
from addict import Dict


# given filename is read
def read_json_file(filename):
    with open(filename, 'r', encoding="utf8") as json_file:
        return json.load(json_file)


# creates a chunk of cities
def divide_city_ids_into_chunks(city_ids,
                                number_of_cities,
                                chunks,
                                chunk_number):
    # determines cities per chunk
    cities_per_chunk = int(math.ceil(number_of_cities / chunks))
    # creates a list for the number of cities per chunk
    city_list = list(range(0, cities_per_chunk))
    # maps the list to determine which cities belong to current chunk
    city_list = list(
        map(lambda x: (x*chunks) + chunk_number, city_list))
    # drops the last number if it turns out to be greater than number_of_cities
    if city_list[-1] >= number_of_cities:
        city_list = city_list[:-1]
    # extracts those cities from the whole list
    cities = list(
        map(lambda i: city_ids[i], city_list))

    return cities


#counts cities in for each country based on chunk
def count_cities_per_country(city_chunk):
    # creating dict
    city_count = Dict()
    # looping through city object and then incrementing for each country encountered
    for city in city_chunk:
        if 'country' in city:
            country_name = city['country']
            if country_name in city_count:
                city_count[country_name] += 1
            else:
                city_count[country_name] = 1
    return city_count


# first creates chunks of the existing data and then maps it onto count_cities_per_country
def map_func(city_ids,number_of_cities,chunks):
    if not chunks:
        return {}
    city_id_chunks = []
    # dividning into chunks - multiprocessing for faster execution
    with concurrent.futures.ProcessPoolExecutor() as executor:
        city_id_futures = [executor.submit(divide_city_ids_into_chunks,
                                        city_ids,
                                        number_of_cities,
                                        chunks,
                                        chunk_number)
                        for chunk_number in range(chunks)]
        for f in concurrent.futures.as_completed(city_id_futures):
            city_id_chunks.append(f.result())
    # applying map - multiprocessing for faster execution
    with concurrent.futures.ProcessPoolExecutor() as executor:
        results = executor.map(count_cities_per_country, city_id_chunks)
        return results


# reduces the output from multiple dictionaries to one
def reduce_func(map_results):
    # creating dict
    city_per_country = Dict()
    # looping through each dictionary and adding city counts to each country
    for result in map_results:
        for key, value in result.items():
            if key in city_per_country:
                city_per_country[key] += value
            else:
                city_per_country[key] = value
    return city_per_country


def main():
    # read file
    city_ids = read_json_file('city_list.json')

    # count the total number of cities
    number_of_cities = float(len(city_ids))
    # chunks hardcoded, modify it to use more than one process
    chunks = 2
    map_results = map_func(city_ids,number_of_cities,chunks)
    reduce_result = reduce_func(map_results)
    print (reduce_result)


if __name__ == '__main__':
    main()